package main

import (
	"github.com/stretchr/testify/require"
	"math"
	"reflect"
	"testing"
)

func TestEasy(t *testing.T) {
	var (
		i interface{}
		v *struct{}
	)

	require.True(t, i == nil)
	require.True(t, v == nil)

	i = v
	require.True(t, i != nil)
}

func TestNilIsNotNil(t *testing.T) {
	var nil = math.NaN()

	require.True(t, nil != nil)
}

func TestInterfaceNotNil(t *testing.T) {
	type Company struct {
		Name string
	}

	var (
		in      interface{}
		company *Company
	)

	// before
	{
		require.True(t, in == nil)
		// expect panic
		require.Panics(t, func() {
			reflect.ValueOf(in).IsNil()
		})
		require.True(t, company == nil)
		require.True(t, reflect.ValueOf(company).IsNil())
	}

	in = company

	// after
	{
		require.True(t, in != nil)
		require.True(t, reflect.ValueOf(in).IsNil())
		require.True(t, in.(*Company) == nil)
	}
}

type Company struct {
	name string
}

type CompanyWrapper struct {
	Company
}

type NameGetter interface {
	Name() string
}

func (c *Company) Name() string {
	if c == nil {
		return ""
	}

	return c.name
}

func TestNilGetter(t *testing.T) {
	{
		var (
			company    *Company
			nameGetter NameGetter = company
		)

		require.Equal(t, "", company.Name())
		require.Equal(t, "", nameGetter.Name())
		require.True(t, nameGetter != nil)
		require.True(t, reflect.ValueOf(nameGetter).IsNil())
	}

	{
		var (
			company    *CompanyWrapper
			nameGetter NameGetter = company
		)

		// will panic
		// require.Equal(t, "", company.Name())
		// will panic
		// require.Equal(t, "", nameGetter.Name())
		require.True(t, nameGetter != nil)
		require.True(t, reflect.ValueOf(nameGetter).IsNil())
	}
}
