package main

import (
	"errors"
	"github.com/stretchr/testify/require"
	"testing"
)

var (
	ErrOxygenSoldOut = errors.New("oxygen sold out")
)

type OxygenProviderResponse interface {
	GetID() uint64
	GetPrice() float64
}

type OxygenProvider = func(quantity float64) (OxygenProviderResponse, error)

type ForestOxygenProvider struct {
	ID    uint64
	Price float64
}

func (f *ForestOxygenProvider) GetID() uint64 {
	return f.ID
}

func (f *ForestOxygenProvider) GetPrice() float64 {
	return f.Price
}

func GetForestOxygen(quantity float64) (*ForestOxygenProvider, error) {
	// 100 magic test number for return error
	if quantity >= 100 {
		return nil, ErrOxygenSoldOut
	}

	return &ForestOxygenProvider{
		ID:    1,
		Price: quantity * 5,
	}, nil
}

func GetFirstOxygen(providers []OxygenProvider, quantity float64) (uint64, float64, error) {
	for _, provider := range providers {
		var response, err = provider(quantity)
		if err != nil {
			// NOP

			// log error

			continue
		}

		return response.GetID(), response.GetPrice(), nil
	}

	return 0, 0, ErrOxygenSoldOut
}

func TestGetFirstOxygen(t *testing.T) {
	{
		id, price, err := GetFirstOxygen(nil, 100)
		require.Equal(t, uint64(0), id)
		require.Equal(t, float64(0), price)
		require.True(t, ErrOxygenSoldOut == err)
	}

	{
		id, price, err := GetFirstOxygen([]OxygenProvider{
			func(quantity float64) (OxygenProviderResponse, error) {
				return GetForestOxygen(quantity)
			},
		}, 100)
		require.Equal(t, uint64(0), id)
		require.Equal(t, float64(0), price)
		require.True(t, ErrOxygenSoldOut == err)
	}

	{
		id, price, err := GetFirstOxygen([]OxygenProvider{
			func(quantity float64) (OxygenProviderResponse, error) {
				return GetForestOxygen(quantity)
			},
		}, 50)
		require.Equal(t, uint64(1), id)
		require.Equal(t, float64(250), price)
		require.True(t, nil == err)
	}
}
