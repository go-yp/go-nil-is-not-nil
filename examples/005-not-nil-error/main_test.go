package main

import (
	"github.com/stretchr/testify/require"
	"testing"
)

type ResponseError string

func (r ResponseError) Error() string {
	return string(r)
}

func Handle() error {
	var err *ResponseError = nil

	// some logic

	return err
}

func TestHandle(t *testing.T) {
	require.True(t, Handle() != nil)
}
