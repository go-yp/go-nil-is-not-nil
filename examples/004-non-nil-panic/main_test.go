package main

import (
	"github.com/stretchr/testify/require"
	"testing"
)

type Company struct {
	name string
}

func (c *Company) Name() string {
	return c.name
}

type NameGetter interface {
	Name() string
}

func TestNotNilPanic(t *testing.T) {
	var (
		compamy *Company   = nil
		in      NameGetter = nil
	)

	require.True(t, compamy == nil)
	require.True(t, in == nil)

	in = compamy

	require.True(t, in != nil)
	require.Panics(t, func() {
		if in != nil {
			_ = in.Name()
		}
	})
}
