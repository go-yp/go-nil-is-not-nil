package main

import (
	"github.com/stretchr/testify/require"
	"testing"
)

type ResponseError string

func (r ResponseError) Error() string {
	return string(r)
}

func Handle() *ResponseError {
	var err *ResponseError = nil

	// some logic

	return err
}

func WrapHandle() error {
	// some wrap logic

	return Handle()
}

func TestWrapHandle(t *testing.T) {
	require.True(t, Handle() == nil)
	require.True(t, WrapHandle() != nil)
}
