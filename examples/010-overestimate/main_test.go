package main

import (
	"github.com/stretchr/testify/require"
	"testing"
)

type Getter interface {
	GetID() int
	GetName() string
}

type Company struct {
	ID   int
	Name string
}

func (c *Company) GetID() int {
	if c == nil {
		return 0
	}

	return c.ID
}

func (c *Company) GetName() string {
	return c.Name
}

type CompanyWrapper struct {
	Company
}

type CompanyPointerWrapper struct {
	*Company
}

func TestOverestimate(t *testing.T) {
	var (
		company               *Company
		companyWrapper        *CompanyWrapper
		companyPointerWrapper *CompanyPointerWrapper
		in                    Getter = nil
	)

	require.True(t, in == nil)

	in = company
	require.True(t, in != nil)
	require.True(t, in.GetID() == 0)
	require.Panics(t, func() {
		in.GetName()
	})
	// if in != nil
	if in != nil && in.GetID() != 0 {
		// do some action
	}

	in = companyWrapper
	require.True(t, in != nil)
	require.Panics(t, func() {
		in.GetID()
	})
	require.Panics(t, func() {
		// if in != nil
		if in != nil && in.GetID() != 0 {
			// do some action
		}
	})

	in = companyPointerWrapper
	require.True(t, in != nil)
	require.Panics(t, func() {
		in.GetID()
	})
	require.Panics(t, func() {
		// if in != nil
		if in != nil && in.GetID() != 0 {
			// do some action
		}
	})
}
