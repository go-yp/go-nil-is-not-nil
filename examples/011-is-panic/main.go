package main

import "fmt"

type Source struct {
}

type Wrap struct {
	Source
}

func (c *Source) ID() int {
	if c == nil {
		return 0
	}

	return 1
}

func main() {
	var (
		source *Source
		wrap   *Wrap
	)

	fmt.Printf("source %d\n", source.ID())

	// is next code panic?
	fmt.Printf("wrap %d\n", wrap.ID())
}
