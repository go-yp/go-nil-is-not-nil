package main

import (
	"fmt"
	"unsafe"
)

type Company struct {
	Name string
}

func main() {
	var (
		in      interface{} = nil
		company *Company    = nil
	)

	fmt.Printf("in == nil %t\n", in == nil)                  // true
	fmt.Printf("%v\n\n", (*[2]uintptr)(unsafe.Pointer(&in))) //

	// in = (*Company)(nil)
	in = company

	fmt.Printf("in == nil %t\n", in == nil)                     // false
	fmt.Printf("in %v\n\n", (*[2]uintptr)(unsafe.Pointer(&in))) //

	in = &Company{
		Name: "Example",
	}

	fmt.Printf("in == nil %t\n", in == nil)                     // false
	fmt.Printf("in %v\n\n", (*[2]uintptr)(unsafe.Pointer(&in))) //

	in = nil

	fmt.Printf("in == nil %t\n", in == nil)                     // true
	fmt.Printf("in %v\n\n", (*[2]uintptr)(unsafe.Pointer(&in))) //
}
