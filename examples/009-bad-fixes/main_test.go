package main

import (
	"github.com/stretchr/testify/require"
	"reflect"
	"testing"
	"unsafe"
)

type NameGetter interface {
	Name() string
}

type Company struct {
	name string
}

func (c *Company) Name() string {
	return c.name
}

func TestNotNilBadFixes(t *testing.T) {
	var (
		company *Company
		in      NameGetter
	)

	require.True(t, company == nil)
	require.True(t, in == nil)

	in = company

	require.True(t, in != nil)

	// if in != nil
	if (*[2]uintptr)(unsafe.Pointer(&in))[1] != 0 {
		// do some action
	}
	require.True(t, (*[2]uintptr)(unsafe.Pointer(&in))[1] == 0)

	// if in != nil
	if !reflect.ValueOf(in).IsNil() {
		// do some action
	}
	require.True(t, reflect.ValueOf(in).IsNil() == true)

	require.Panics(t, func() {
		var in interface{}

		reflect.ValueOf(in).IsNil()
	})

	require.Panics(t, func() {
		var in struct{}

		reflect.ValueOf(in).IsNil()
	})

	if in != nil && !reflect.ValueOf(in).IsNil() {
		// do some action
	}
}
