package main

import (
	"fmt"
)

type Tree struct {
	left  *Tree
	right *Tree
	value int
}

func (t *Tree) Sum() int {
	if t == nil {
		return 0
	}

	return t.value + t.left.Sum() + t.right.Sum()
}

func main() {
	{
		var data map[string]string = nil

		fmt.Printf("map[string]string len(data) = %d\n", len(data))

		for key, value := range data {
			fmt.Printf("key = %q, value = %q\n", key, value)
		}
	}

	{
		var data []string = nil

		fmt.Printf("[]string len(data) = %d, cap(data) = %d\n", len(data), cap(data))

		for key, value := range data {
			fmt.Printf("key = %d, value = %q\n", key, value)
		}
	}

	{
		var tree *Tree = nil

		fmt.Printf("Tree.Sum() = %d\n", tree.Sum())
	}
}
